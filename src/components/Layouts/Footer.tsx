import { shuffle } from 'lodash'
import React from 'react'

import Nav from '@/components/JokeCats'
import catsdata from '@/data/cats'

function Footer({ hide }: { hide?: boolean }) {
  return (
    <footer className="relative">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
        <path
          fill="#2d3749"
          d="M0 288l120-16c120-16 360-48 600-58.7 240-10.3 480-.3 600 5.4l120 5.3v96H0z"
        ></path>
      </svg>

      <div className="absolute top-10 w-full">
        {!hide && <Nav cats={shuffle(catsdata).slice(0, 3)} />}
      </div>
      <div className="absolute bottom-2 w-full text-center">
        © {new Date().getFullYear()}, Built by RudixOps with ❤️
      </div>
    </footer>
  )
}

export default Footer
