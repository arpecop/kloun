import dynamic from 'next/dynamic'
import Script from 'next/script'
import type { ReactNode } from 'react'

const Logo = dynamic(() => import('@/components/Layouts/Header'), {
  ssr: false,
})
const Footer = dynamic(() => import('@/components/Layouts/Footer'), {
  ssr: false,
})

type IMainProps = {
  meta: ReactNode
  children: ReactNode
  hideFooter?: boolean
  title?: string
}

const Main = (props: IMainProps) => (
  <>
    {props.meta}
    <Logo title={props.title} />
    <div className="-mt-8 text-gray-400 sm:-mt-14  md:-mt-12">
      <div className="container mx-auto   px-2 pb-20 sm:px-4 md:px-8">
        {props.children}
      </div>
    </div>
    <Script
      src="https://www.googletagmanager.com/gtag/js?id=G-99VWGKYVS6"
      strategy="afterInteractive"
    />
    <Footer hide={props.hideFooter} />
    <Script id="google-analytics" strategy="afterInteractive">
      {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'GA_MEASUREMENT_ID');
        `}
    </Script>
  </>
)

export { Main }
