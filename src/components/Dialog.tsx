/* eslint-disable no-underscore-dangle */
import React, { useEffect, useState } from 'react'

import FacebookShare from './FacebookShare'
import { JokeThumbnail } from './JokeThumbnail'

const Dialog = ({
  joke,
  id,
  show,
}: {
  joke: string
  id: string
  show: boolean
}) => {
  const [closedInternal, setClosedInternal] = useState(show)

  useEffect(() => {
    setClosedInternal(false)
  }, [show])
  const close = () => {
    document.body.style.overflow = 'auto'
    setClosedInternal(true)
  }
  if (!closedInternal && joke) {
    return (
      <div
        className="fixed top-0 left-0 z-10 h-screen w-screen  cursor-pointer overflow-auto bg-black/30 backdrop-blur-sm"
        onClick={close}
      >
        <div className="flex flex-col items-center">
          <svg
            className="h-12 w-12 self-end"
            xmlns="http://www.w3.org/2000/svg"
            version="1.1"
            viewBox="0 0 744.09 1052.4"
          >
            <g>
              <path
                stroke="#000"
                strokeWidth="5.524"
                d="M720.418 507.697a347.228 347.228 0 11-694.445 0 347.228 347.228 0 11694.445 0z"
              ></path>
              <g
                fill="none"
                stroke="#fff"
                strokeLinecap="round"
                strokeWidth="133.87"
                transform="translate(47.587 10.944) scale(.91837)"
              >
                <path d="M176.51 362.87L532.64 719"></path>
                <path d="M532.64 362.87L176.51 719"></path>
              </g>
            </g>
          </svg>
          <JokeThumbnail
            item={{ joke, id, cat: '', _id: '' }}
            showcats={false}
            short={false}
            hideReadMore={true}
          />

          <FacebookShare id={id} />
        </div>
      </div>
    )
  }
  return null
}

export default Dialog
