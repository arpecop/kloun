import type { Cat } from '../components/JokeCats'

const catsdata: Cat[] = [
  {
    rotate: 0,
    count: 0,
    cat: 'Разни',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Адвокати',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Бай Ганьо',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Бизнесмени',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Бисери',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Блондинки',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Борци',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Военни',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'В ресторанта',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Габровски',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Гадории',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Годеници',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Деца',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Други',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Животни',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Иванчо и Марийка',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Индианци и Каубои',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Канибали',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Киркор и Гарабед',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Коледа и Нова година',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Комунистически',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Лекари',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Ловци',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Луди',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Любими Герои',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Мечо Пух',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Митничари',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Младоженци',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Монаси',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Морски',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Мръсни',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Нане и Вуте',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Овчари',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Петка и Чапаев',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Пиянски',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Подсъдими',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Пожелания',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Политически',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Полицаи',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Поручик Ржевски',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Програмисти',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Проститутки',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Професионални',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Радио Ереван',
  },

  {
    rotate: 0,
    count: 0,
    cat: 'Расистки',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Рибари',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Семейни',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Слаботелесни',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Спортни',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Студентски',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Тъпизми',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Тъщи',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Ученически',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Цигани',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Червената шапчица',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Черен хумор',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Чукчи',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Шерлок Хоумс',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Шофьори',
  },
  {
    rotate: 0,
    count: 0,
    cat: 'Щирлиц',
  },
]
export default catsdata
