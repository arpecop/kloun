// import { useRouter } from 'next/router';

import type { NavProps } from '@/components/JokeCats'
import Nav from '@/components/JokeCats'
import { Main } from '@/components/Layouts/Main'
import { Meta } from '@/components/Layouts/Meta'
import data from '@/data/structure'

import catsdata from '../data/cats'

const Index = (props: NavProps) => {
  // const router = useRouter();

  return (
    <Main
      hideFooter
      meta={
        <Meta
          title="Вицове и забавни котки и мемета"
          description="Вицове и забавни котки и мемета"
        />
      }
    >
      <Nav cats={props.cats} />
    </Main>
  )
}

//    <Nav cats={remap} />
export async function getStaticProps() {
  const obj = await data()
  const remapcats = catsdata.map(({ cat }) => {
    const rotate = Math.floor(Math.random() * 6)
    return { cat, count: obj[cat].size, rotate }
  })

  return {
    props: { cats: remapcats },
  }
}

export default Index
